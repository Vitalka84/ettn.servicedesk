# еТТН Сервіс Деск

Цей репозиторій створено для обміну матеріалами по проекту еТТН
Будь-які проблеми і питання можна задавати в категорії Issues даного репозиторію


# Специфікація і статус розробки

Специфікація в розробці (candidate):  https://app.swaggerhub.com/apis/ett99/ettn/1.0.5
 
Специфікація в тестування (staging): https://app.swaggerhub.com/apis/ett99/ettn/1.0.4
 

# Середовище для тестування

Специфікація з описами необхідних workflows: 
https://app.swaggerhub.com/apis/ett99/ettn/1.0.4

Базовий staging сервіс: https://stage.ettn.wilded.io:443/api
Для авторзації в API використовується HTTP заголовок apikey.

# Як отримати тестовий доступ, або як задати питання
Відкрийте Issue в даному репозиторії і розкажіть про компанію, цілі і задачі Вашого бізнесу
https://gitlab.com/wldd/ettn/ettn.servicedesk/issues

Нажаль, ми не надаємо підтримку в чатах, телефонах і мессенджарах, віддаючи перевагу асинхронному спілкуванню


# Приклад запиту до сервісу

```
curl -X POST \
  http://localhost:9090/ettn \
  -H 'Content-Type: application/json' \
  -H 'cache-control: no-cache' \
  -d '{
	"status": "originatorSigned",
	"payload": {
		"tn": "АА1234ББ",
		"tonnage": 20012,
		"subtype": "generic"
	},
	"document": {
		"mimeType": "application/pkcs7-mime",
		"size": 32321,
		"url": "https://shared.storage.tis.ioio/QWERTY",
		"signature": "NmMxYzUyZjA1YzQzMWFkY2YxYzkyMGE2NGVhYTRiZTgxZTYxYmFlYTM3OWYzNjllZWFmMGRmMzM0YzA0MTNiZiAgLQo=",
		"signatureUrl": "https://shared.storage.tis.ioio/QWERTYSIG",
		"type": "ttn"
	}
}'
```
